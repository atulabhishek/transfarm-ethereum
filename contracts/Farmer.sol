pragma solidity >=0.4.22 <0.6.0;

/// @title Accepts input from the farmer about the product as well as the QC(Quality Control Board) address.  
contract Farmer {

    struct Document{
        address payable farmer;
        address payable qc;
        bool openState;
        bool verifiedByQc;
        string productName;
        uint32 quantity;
        uint32 price;
    }
    
    Document[] public documents;

    function setNewProduct(string memory _productName, uint32 _quantity, uint32 _price, address payable _qc) public payable{
        documents.push(Document(msg.sender, _qc, true, false, _productName, _quantity, _price));
        uint value = msg.value;
        address payable receiver = documents[0].qc;
        receiver.transfer(value);
    }

    function getFarmer() public view returns(address){
        return documents[0].farmer;
    }

    function getOpenState() public view returns(string memory){
        if (documents[0].openState == true){
            return "true";
        }else {
            return "false";
        }
    }

    function getQcVerificationStatus() public view returns(string memory){
        if (documents[0].verifiedByQc == true){
            return "true";
        }else {
            return "false";
        }
    }

    function getProductName() public view returns(string memory){
        return documents[0].productName;
    }

    function getQuantity() public view returns(uint32){
        return documents[0].quantity;
    }

    function getPrice() public view returns(uint32){
        return documents[0].price;
    }
}


/// @title Accepts input from the QC whether to pass or fail the product.
contract Qc is Farmer {

    function qcPassed() public {
        require(msg.sender == documents[0].qc, "You are not one of the verified QC's.");
        require(documents[0].openState == true, "The contract is rejected, no future alterations possible!!");
        documents[0].verifiedByQc = true;
        documents[0].openState = false;
    }

    function qcFailed() public {
        require(msg.sender == documents[0].qc, "You are not one of the verified QC's.");
        require(documents[0].openState == true, "The contract is rejected, no future alterations possible!!");
        documents[0].openState = false;
    }
}


/// @title The buyer is allowed to pay and buy the product only if it has been approved by QC.
contract Buyer is Qc{

    function buy(uint32 _quantity) public payable{
        require(documents[0].verifiedByQc == true, "The contract has not been passed by QC yet!!");
        documents[0].quantity -= _quantity;
        uint value = msg.value;
        address payable receiver = documents[0].farmer;
        receiver.transfer(value);
    }
}
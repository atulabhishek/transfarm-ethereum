ProjectAuthor 


Created By: Atul Abhishek, Kshitij Raj and Mekha Anil


#########

IMPORTANT: Buyer.sol is the main contract.

#########


Description:

Agriculture is one of the most prominent sector in the world, but the gap of the price at which the farmer sell its produce and the final price at which the people buys it is significant.


Issues with the Farmer : 

Lack of Options (Marketing and Sales) - Being the main focus on farming, not knowing, where to sell the product - Resulting to sell only the the Middle Men - Hence Exploitation


Issues with the Market :

Lack of Reach to the rural or farming population of the country and if reached, not knowing the proper quality of the received product(s).


Common Issue : Transparency with the system - Both the parties doesn't know the price of vice versa. 

Therefore, In this project we aim to build a system where the middle-man is removed from the system and trust factor is established by including the Quality Control Board (QC). 

In our system, the farmer(s) who produces good, is able to sell his produce directly to the Marketplace, ensuring the proper approval from the Quality Control Board (QCB)


An entirely transparent and decentralised marketplace can be made from this, due to an easy, safe and trust establishment, providing quick payment solutions to the farmer for their goods, at a considerable price.



#############################

Installation Instructions

#############################

Please ensure that your computer has the following prerequisites installed before proceeding:

-geth
-truffle
-metamask-extension (preferably in google chrome) [with at least 1 ether in a couple of the accounts in Ropsten/Rinkeby/localhost]


1) Clone the repo into a local folder.
2) From the terminal navigate to 'ethereum-transfarm/'.


[For local execution on geth "private ethereum chain"] 

1) Make sure geth is running.
2) For this project, 3 accounts are required(having initial ethers). So, run the "geth --datadir data account new" command 3 times and copy all the 3 address's in genesis.json file appending "0x" in front of the address.  
3) Create genesis block using the command "geth --datadir data init genesis.json".
4) To make the network live, use the command geth --identity "miner" --networkid 4002 --datadir data --nodiscover --mine --rpc --rpcport "8545" --port "8191" --unlock 0 --ipcpath "/.ethereum/geth.ipc" --rpccorsdomain "*" --rpcapi "db,eth,net,web3,personal"
5) After this command enter the password given for the first account in genesis.json.
   NOTE: RPC port is 8545 in the above command (Used for Metamask and truffle connection)
6) Now the local private blockchain instance is ready!!
7) Following steps will deploy the smart contracts on the blockchain using Truffle as the deployment tool.
    a) Navigate to project directory 'ethereum-transfarm/'
    a) Open truffle-config.json file and make sure the port number is 8585 (in order to connect to private ethereum chain).
    b) In the terminal type "truffle compile" to compile the smart contracts.
    c) In the terminal type "truffle migrate" to deploy the smart contracts.
    d) From the terminal output of above command, copy the "contract address" of the "Buyer" contract for later use. 
8) Follow the steps below to interact with the deployed contracts through GUI.
    a) Start the local HTTP server on your machine  "python -m http.server 7200" (command for windows machine) and access the HTML file at http://localhost:7200/gui.html.
    a) Replace the contract address in the HTML file with the new address of the "Buyer" contract(from previous step).
    b) Replace the ABI in the HTML file with the one in "Buyer.json" file under build directory.
    c) Run the HTML file to interact with the smart contract!!

NOTE: The 3 acconts created in the genesis block can be imported in Metamask extension using keystore files for payment purposes through the GUI.


[For execution on Ropsten "public ethereum chain"]

1) The contracts in this project are already deployed on Ropsten at address "0xfe9c62756906acf4d08a0968d02b3cf9e0d1446c".
2) The etherscan URL for the above contract is: https://ropsten.etherscan.io/tx/0xbaf2fcbc5e6db6b5013748a4b0dc1f6411ebf5d599fef5be4e4204db636862c0
2) Change the web3 provider to web3 = new Web3(new Web3.providers.HttpProvider("ropsten.infura.io/v3/9dac1c989ec44db0aaf109792ef0dc49"))
3) To access the contract, just change the contract address in the HTML file to above address. 
4) Start the local HTTP server on your machine  "python -m http.server 7200" (command for windows machine) and open the HTML file on the local server http://localhost:7200/gui.html


#############################

Note: Backup File is provided in the project.

#############################



#############################

Extra Functionalities (implemented but not used in this project) :
To demonstrate our understanding of Solidity Libraries :

#############################

1) Ownable.sol :  The ownable contract has an owner address, and provided basic authorisation control functions, this simplifies the implementations of “usr permissions”


2) Safemath.sol : Safemath library is for unsigned mathematical operations with safety checks that revert on error
const Farmer = artifacts.require("Farmer");
const Qc = artifacts.require("Qc");
const Buyer = artifacts.require("Buyer");

module.exports = function(deployer) {
  deployer.deploy(Farmer);
  deployer.deploy(Qc);
  deployer.deploy(Buyer);
};

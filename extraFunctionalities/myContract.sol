pragma solidity >=0.4.22 <0.6.0;

import "./ownable.sol";
import "./safemath.sol";

contract myContract is Ownable {

    using SafeMath for uint256;

    function multiply(uint256 a, uint256 b) public returns(uint256){
      uint256 c = mul(a, b);
      return c;    
    }

    function divide(uint256 a, uint256 b) public returns(uint256){
      uint256 d = div(a, b);
      return d;    
    }
    
    function addition(uint256 a, uint256 b) public returns(uint256){
      uint256 e = add(a, b);
      return e;
    }

    function subtract(uint256 a, uint256 b) public returns(uint256){
      uint256 f = sub(a, b);
      return f;    
    }
}
